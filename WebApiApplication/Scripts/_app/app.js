﻿/// <reference path="../angular.js" />

app = angular.module("spa", ['ngResource', 'ngRoute']);

app.factory('employeeService', function ($resource) {
    return $resource('/api/employees/:id',
        { id: '@Id' },
        {
            update: { method: 'PUT' }
        });
});

app.factory('departmentService', function ($resource) {
    return $resource('/api/departments/:id',
        { id: '@Id' },
        {
            update: { method: 'PUT' }
        });
});

app.controller('employeeController', function ($scope, employeeService, departmentService) {

    $scope.settings = {
        title: "Employess"
    };

    $scope.errors = [];

    $scope.employees = employeeService.query();
    $scope.departments = departmentService.query();

    $scope.employee = {
        Id: '',
        Name: '',
        Email: '',
        Position: '',
        DepartmentId: 0
    };


    $scope.saveEmployee = function () {
        if ($scope.employee.Id > 0) {
            employeeService.update($scope.employee, $scope.refreshEmployees, $scope.errorMessage);
        } else {
            $scope.employee.Id = 0;
            employeeService.save($scope.employee, $scope.refreshEmployees, $scope.errorMessage);
        }
    }

    $scope.deleteEmployee = function (emp) {
        employeeService.delete(emp, $scope.refreshEmployees, $scope.errorMessage);
    };

    $scope.errorMessage = function (response) {
        var errors = [];
        for (var key in response.data.ModelState) {
            for (var i = 0; i < response.data.ModelState[key].length; i++) {
                errors.push(response.data.ModelState[key][i]);
            }
        }
        $scope.errors = errors;
    };
    
    $scope.refreshEmployees = function () {
        $scope.employees = employeeService.query();
        $('#modal-dialog').modal('hide');
    }

    $scope.selectEmployee = function (emp) {
        $scope.employee = emp;
        $scope.showDialog("Edit Employee");
    }

    $scope.showAddEmployeeDialog = function () {
        $scope.clearEmployee();
        $scope.showDialog("Add New Employee");
    };

    $scope.showDialog = function (title) {
        $scope.errors = [];
        $scope.settings.title = title;
        $scope.departments = departmentService.query();
        $('#modal-dialog').modal('show');
    }

    $scope.clearEmployee = function () {
        $scope.employee = {
            Id: '',
            Name: '',
            Email: '',
            Position: '',
            Bio: '',
            DepartmentId: 0
        };
    }
});


app.controller('departmentController', function ($scope, departmentService) {

    $scope.settings = {
        title: "Departments"
    };

    $scope.errors = [];

    $scope.departments = departmentService.query();

    $scope.department = {
        Id: '',
        Name: ''
    };


    $scope.saveDepartment = function () {
        if ($scope.department.Id > 0) {
            departmentService.update($scope.department, $scope.refreshDepartments, $scope.errorMessage);
        } else {
            $scope.department.Id = 0;
            departmentService.save($scope.department, $scope.refreshDepartments, $scope.errorMessage);
        }
    }

    $scope.deleteDepartment = function (dpmt) {
        departmentService.delete(dpmt, $scope.refreshDepartments, $scope.errorMessage);
    };

    $scope.errorMessage = function (response) {
        var errors = [];
        for (var key in response.data.ModelState) {
            for (var i = 0; i < response.data.ModelState[key].length; i++) {
                errors.push(response.data.ModelState[key][i]);
            }
        }
        $scope.errors = errors;
    };

    $scope.refreshDepartments = function () {
        $scope.departments = departmentService.query();
        $('#modal-dialog').modal('hide');
    }

    $scope.selectDepartment = function (dpmt) {
        $scope.department = dpmt;
        $scope.showDialog("Edit Department");
    }

    $scope.showAddDepartmentDialog = function () {
        $scope.clearDepartment();
        $scope.showDialog("Add New Department");
    };

    $scope.showDialog = function (title) {
        $scope.errors = [];
        $scope.settings.title = title;
        $('#modal-dialog').modal('show');
    }

    $scope.clearDepartment = function () {
        $scope.department = {
            Id: '',
            Name: ''
        };
    }
});


app.config(function ($routeProvider) {

    $routeProvider        
        .when('/employees', {
            templateUrl: '/Views/Home/Employees.html',
            controller: 'employeeController'
        })
        .when('/departments', {
            templateUrl: '/Views/Home/Departments.html',
            controller: 'departmentController'
        });
});